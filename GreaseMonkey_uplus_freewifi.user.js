// ==UserScript==
// @name         auto skip U+ free wifi AD
// @namespace    https://bitbucket.org/hwiorn/gm-uplus-freewifi
// @version      1.1
// @description  auto skip U+ free wifi AD
// @author       hwiorn@gmail.com
// @match        https://uplus.kr:8443/images/NS/banner/uzone/*
// @grant        none
// ==/UserScript==

(function() {
    'use strict';

    if(typeof videoPlayed !== 'undefined') videoPlayed = true;
    if(typeof videoEnded !== 'undefined') videoEnded = true;
    if(typeof embedVideoPlayTime !== 'undefined') embedVideoPlayTime = 1;
    if(typeof certifyEnded !== 'undefined') certifyEnded = true;

    fnCheckAuth();
})();
